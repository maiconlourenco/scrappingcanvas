import os
import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup


class CanvasBot:

    def __init__(self):
        """"
        Configura o chrome, para não abrir o navegador e rodar headless;
        Cria o driver para fazer o scrapping;
        """
        chrome_options = Options()

        # Se você quer ver abrir o navegador e fazer o login e o scrapping comente estas três linhas abaixo;
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        ###

        self.browser = webdriver.Chrome(
            options=chrome_options, executable_path="./chromedriver")

    def login(self, username: str, password: str):
        """
        recebe username, password e faz login no canvas.
        """
        print('Login started...')

        self.browser.get('https://alunos.kenzie.com.br/login/canvas')

        time.sleep(1)

        username_input = self.browser.find_element_by_css_selector(
            'input#pseudonym_session_unique_id.ic-Input')
        username_input.send_keys(username)

        password_input = self.browser.find_element_by_css_selector(
            'input#pseudonym_session_password.ic-Input')
        password_input.send_keys(password)

        login_button = self.browser.find_element_by_css_selector(
            'button.Button.Button--login')
        login_button.click()

        print('successfully logged!')
        time.sleep(1)
        return

    def search_pages(self):
        """
        Depois de logado, procura pelo título dos contéudos, e verifica se está desbloqueado,
        e se o contém conteúdo extraível, se estiver ok, chama get_content() para coletar o contéudo;
        Tambêm inicia um cronometro para registrar o tempo total de todos os processos.
        """
        print('Canvas Web Scraping started!')
        start_time = time.time()
        print('Please await...',  end='')

        result = list()
        page_source = self.browser.page_source
        parser = BeautifulSoup(page_source, 'html.parser')

        self.browser.execute_script(
            "window.scrollTo(0, document.body.scrollHeight)")

        time.sleep(2)

        os.chdir(os.getcwd() + '/files')

        items = parser.find_all('div', {'class': 'title_2eX6'})
        for item in items:
            elem = item.select_one('.fOyUs_bGBk')
            if elem.name == 'a':
                url = 'https://alunos.kenzie.com.br' + elem.get('href')
                title = elem.select('a > span > span')[1].text

                content_file = self.get_content(title, url)
                if content_file:
                    result.append(content_file)

        print('running time: ', time.time() - start_time)
        print('Successfully completed processes, please check /files folder.')

        # Para fechar o navegador automaticamente descomente a linha abaixo
        # self.browser.quit()
        print('Thanks for using this app ;)')

        return result

    def get_content(self, title, url):
        """
        Coleta o contéudo dos servidores da amazon, e escreve em um arquivo .txt
        """

        self.browser.get(url)
        page_source = self.browser.page_source
        parser = BeautifulSoup(page_source, 'html.parser')
        content = parser.select('iframe')[-1]['src']

        self.browser.get(content)
        page_source = self.browser.page_source
        parser = BeautifulSoup(page_source, 'html.parser')
        content = parser.select('body')

        if content[0].text != '':
            file_title = title.lower().replace(' ', '_').replace(':', '')

            with open(file_title + '.txt', "w") as content_file:
                content_file.write(content[0].text)

            return file_title
