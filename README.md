# Canvas Web crawler | 

Este crawler, faz login no canvas e ao entrar faz um scroll limitado ao tamanho a viewport do seu navegador,
depois coleta todos os conteúdos do canvas pelo seu título que não estejam bloqueados, 
entra em cada um deles e coleta o conteúdo principal, e o escreve em um arquivo .txt na pasta files.
Não compartilhe suas credencias, nem o conteúdo do canvas.

## Guia

* Insira suas credencias no arquivo main.py
* Email e senha usados no canvas.
* Rodar o arquivo main.py para iniciar o Scrapping.
  

---
**OBS:** o processo pode demorar um pouco aguarde um momento, 
dependendo da sua conexão e do servidor do canvas, a primeira vez pode demorar bastante,
mas as próximas execuções são mais rápidas.