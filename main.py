from core.chrome_crawler import CanvasBot

if __name__ == '__main__':
    bot = CanvasBot()

    # ATENÇÂO! coloque aqui seu email e senha do canvas para o bot fazer login, e não compartilhe.
    bot.login('seuemail@gmail.com', 'sua-senha-aqui')
    result = bot.search_pages()
    print(result)
